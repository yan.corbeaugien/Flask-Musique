from flask import Flask
app = Flask(__name__)
app.debug = True
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

from flask.ext.script import Manager
manager = Manager(app)

from flask.ext.bootstrap import Bootstrap
Bootstrap(app)

import os.path
def mkpath(p):
	return os.path.join(
		os.path.dirname(__file__),
		p)

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = (
	'sqlite:///'+mkpath('../myapp.db'))
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "1dffa893-bac3-4cac-8252-8041f1f1bd3d"

from flask.ext.login import LoginManager
login_manager = LoginManager(app)
login_manager.login_view = "login"