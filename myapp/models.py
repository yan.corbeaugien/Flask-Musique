from .app import db


association_Album_Genre = db.Table('album_genre',
    db.Column('album_id', db.Integer, db.ForeignKey('album.id')),
    db.Column('genre_id', db.Integer, db.ForeignKey('genre.id'))
)

class Genre(db.Model):
	__Genre__ = 'genre'
	id = db.Column(db.Integer, primary_key=True)
	name=db.Column(db.String(100))
	def __repr__(self):
		return "<Genre (%d) %s>" % (self.id, self.name)

class Group(db.Model):
	__Group__ = 'group'
	id = db.Column(db.Integer, primary_key=True)
	name=db.Column(db.String(100))
	def __repr__(self):
		return "<Group (%d) %s>" % (self.id, self.name)

class Album(db.Model):
	__Album__ = 'album'
	id = db.Column(db.Integer, primary_key=True)
	title=db.Column(db.String(100))
	year=db.Column(db.String(4))
	img=db.Column(db.String(100))
	parent=db.Column(db.String(100))
	genre = db.relationship("Genre",secondary=association_Album_Genre,backref=db.backref("albums",lazy="dynamic"))

	group_id=db.Column(db.Integer, db.ForeignKey("group.id"))
	group=db.relationship("Group",backref=db.backref("album",lazy="dynamic"))


from flask.ext.login import UserMixin
class User(db.Model, UserMixin):
	username = db.Column(db.String(50), primary_key=True)
	password = db.Column(db.String(64))

	def get_id(self):
		return self.username

	def get_user(name):
		return User.query.get_or_404(name)

from .app import login_manager
@login_manager.user_loader
def load_user(username):
	return User.query.get(username)
# ==== REQUEST ==== #

#Album
def get_sample():
	return Album.query.limit(24).all()

ALBUM_PER_PAGE = 30
def get_sample_page(page):
	return Album.query.paginate(page, ALBUM_PER_PAGE, False)

def get_albums():
	return Album.query.all()

def get_album(id):
	return Album.query.get_or_404(id)

def get_genres():
	return Genre.query.all()

def get_genre_by_name(txt):
	return Genre.query.filter(Genre.name.like(txt)).first()
	
def rechercheAlbum(name):
	return Album.query.filter(Album.title.like('%'+name+'%'))
	
def get_sample_genre():
	return Genre.query.limit(24).all()
	
def ajouterMusic(author,img,parent,releaseYear,title,genre):
	musique = Album(group=author,img=img,parent=parent,year=releaseYear,title=title,genre=genre)
	db.session.add(musique)
	db.session.commit()
