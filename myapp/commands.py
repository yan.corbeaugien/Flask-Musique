from .app import manager, db

@manager.command
def loaddb(filename):
	db.create_all()
	import yaml
	albums=yaml.load(open(filename))

	from .models import Genre,Group,Album

	genres={}
	groups={}
	albumSet=set()
	
	def newGenre(g):
		g = g.lower().replace('-',' ').strip('?",').replace('. ',' ').replace('.',' ').replace('alt ','alternative ').replace('[citation needed]','')
		if g=='' or g.startswith('length') or g.startswith('42:') or g.startswith('60:'):
			g = 'unknown'
		return g.title()

	for album in albums:
		for g in album["genre"]:
			g = newGenre(g)
			if g not in genres:
				objt=Genre(name=g)
				db.session.add(objt)
				genres[g]=objt

		if album["by"] not in groups:
				objt=Group(name=album["by"])
				db.session.add(objt)
				groups[album["by"]]=objt

		objt=Album(title = album["title"],
				year = album["releaseYear"],
				img = album["img"],
				genre = [genres[newGenre(g)] for g in album["genre"]],
				parent = album["parent"],
				group = groups[album["by"]])
		db.session.add(objt)
	db.session.commit()




@manager.command
def newuser(username, password):
	''' Ajoute un nouvel utilisateur '''

	from .models import User
	from hashlib import sha256
	m = sha256()
	m.update(password.encode())
	u = User(username=username , password=m.hexdigest())
	db.session.add(u)
	db.session.commit()


@manager.command
def passwd(username, pwd):
	''' Modifie un mot de passe '''
	from .models import User
	from hashlib import sha256
	m = sha256()
	m.update(pwd.encode())
	u = User.get_user(username)
	u.password = pwd
	db.session.commit()
