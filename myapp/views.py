#======================= Les vues =======================
from .app import app
from flask import render_template
from .models import *
from flask.ext.paginate import Pagination

@app.route("/")
@app.route("/<int:page>")
def home(page=1):
    search = False
    pagination = Pagination(page=page, total=Album.query.count()//3, search=search, css_framework='foundation')
    return render_template("home.html",title="The Beat Pit",albums=get_sample_page(page), pagination=pagination)

@app.route("/album/<int:id>")
def one_album(id):
    a=get_album(id)
    return render_template(
        "album.html",
        title="Les albums",
        album=get_album(id),
        genres = ','.join([ g.name for g in a.genre ])
        )

from flask.ext.login import logout_user

@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route("/search/",methods=['post'])
def recherche():
	elem = request.form['word']
	if elem == '':
		elem = 'Recherche vide'
	return render_template("rechercheAlbum.html",rechercheAlbum=rechercheAlbum(elem),title="The Beat Pit")


#======================= Les formulaires =======================
from flask.ext.wtf import Form
from wtforms import StringField, HiddenField, PasswordField
from wtforms.validators import DataRequired
from flask.ext.login import login_required

class AlbumsForm(Form):
    id = HiddenField('id')
    title = StringField('Titre', validators=[DataRequired()])
    artiste = StringField('Artiste', validators=[DataRequired()])
    year = StringField('Annee', validators=[DataRequired()])
    img = StringField('Couverture')
    genre = StringField('Genre', validators=[DataRequired()])
    parent = StringField('Parent', validators=[DataRequired()])

@app.route("/edit/album/<int:id>")
@login_required
def edit_album(id):
    a = get_album(id)
    g = ','.join([ g.name for g in a.genre ])
    f = AlbumsForm(id=a.id, title=a.title, artiste=a.group, year=a.year, img=a.img , genre=g, parent=a.parent)
    return render_template(
        "edit-album.html",
        title="Les albums",
        album=a,
        genres=get_genres(),
        form=f)

@app.route("/create/album/")
def create_album():
    a = None
    f = AlbumsForm()
    return render_template(
        "create-album.html",
        title="The Beat Pit",
        album=a,
        form=f)

from .models import User
from hashlib import sha256

class LoginForm(Form):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None

from flask.ext.login import login_user, current_user
from flask import request,url_for, redirect, flash

@app.route("/login/", methods=("GET", "POST",))
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            next = f.next.data or url_for("home")
            return redirect(next)
    return render_template(
        "login.html",
        form=f,title="The Beat Pit")


#======================= Les actions des formulaires =======================
from flask import url_for, redirect, flash
from .app import db
from .models import Album

@app.route("/save/album/", methods=("POST",))
def save_album():
    a = None
    f = AlbumsForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_album(id)
        a.title = f.title.data
        a.year = f.year.data
        a.img = f.img.data
        a.group = f.artiste.data
        gn=[]
        for genre in f.genre.data.split(','):
            genre = get_genre_by_name(genre)
            if genre:
                gn.append(genre)
        a.genre=gn
        a.parent = f.parent.data
        db.session.commit()
        return redirect(url_for('one_album', id=a.id))
    a = get_author(int(f.id.data))
    return render_template(
        "edit-album.html",
        author=a,
        form=f)

@app.route("/new/album/", methods=("POST",))
def new_album():
    f = AlbumsForm()
    if f.validate_on_submit():
        t = f.title.data
        y = f.year.data
        art = f.artiste.data
        i = f.img.data
        gn=[]
        for genre in f.genre.data.split(','):
            genre = get_genre_by_name(genre)
            if genre:
                gn.append(genre)
        p = f.parent.data
        o = Album(title=t,year=y,img=i,genre=gn,parent=p,artiste=art)
        db.session.add(o)
        db.session.commit()
        print('album ajouté')
        return redirect(url_for('home'))
    else:
        return render_template("edit-album.html",
        album=a,
        form=f)
    
